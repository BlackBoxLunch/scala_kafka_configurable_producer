import java.util.Properties
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import scala.util.Random
import org.slf4j.LoggerFactory


object KafkaMessageProducer {

  private val logger = LoggerFactory.getLogger(this.getClass)

  def main(args: Array[String]): Unit = {
    val props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    logger.info("Starting Kafka Producer...")
    val producer = new KafkaProducer[String, String](props)
    val topics = Array("claim_submissions", "eligibility_checks", "payment_updates")

    val messagesPerMillisecond = args(0).toInt
    val maxMessages = args(1).toInt
    var messageCount = 0

    while (messageCount < maxMessages) {
      for (_ <- 1 to messagesPerMillisecond if messageCount < maxMessages) {
        val (topic, message) = generateMessage(topics(Random.nextInt(topics.length)))
        //val message = generateMessage(topics(Random.nextInt(topics.length)))
        //val record = new ProducerRecord[String, String]("your_topic", message)
        val record = new ProducerRecord[String, String](topic, message) // Use the topic from the tuple

        logger.info("Sending a message...")
        producer.send(record)
        logger.info("Message sent successfully")        
        messageCount += 1
      }
      Thread.sleep(1000)  // Sleep for 1 millisecond
    }

    producer.close()
    logger.info("Producer closed successfully.")

  }

def generateMessage(topic: String): (String, String) = {
  val message = topic match {
    case "claim_submissions" => s"""{"transaction_id":"${Random.nextInt(10000)}","transaction_type":"claim_submission","provider_id":"PR${Random.nextInt(10000)}","patient_id":"PT${Random.nextInt(10000)}","service_date":"2024-05-${Random.nextInt(30)+1}","amount_claimed":${Random.nextDouble() * 1000.0},"diagnosis_code":"D${Random.nextInt(100)}","service_code":"S${Random.nextInt(500)}"}"""
    case "eligibility_checks" => s"""{"transaction_id":"${Random.nextInt(10000)}","transaction_type":"eligibility_check","provider_id":"PR${Random.nextInt(10000)}","patient_id":"PT${Random.nextInt(10000)}","service_date":"2024-05-${Random.nextInt(30)+1}","policy_number":"PL${Random.nextLong()}"}"""
    case "payment_updates" => s"""{"transaction_id":"${Random.nextInt(10000)}","transaction_type":"payment_update","provider_id":"PR${Random.nextInt(10000)}","payment_id":"PY${Random.nextInt(10000)}","amount_paid":${Random.nextDouble() * 1000.0},"payment_date":"2024-05-${Random.nextInt(30)+1}","payment_status":${if (Random.nextBoolean()) "\"completed\"" else "\"pending\""}"}"""
  }
  (topic, message)
}

}
