// The simplest possible sbt build file is just one line:

// scalaVersion := "2.13.12"
scalaVersion := "3.4.1"
// That is, to create a valid sbt build, all you've got to do is define the
// version of Scala you'd like your project to use.

// ============================================================================

// Lines like the above defining `scalaVersion` are called "settings". Settings
// are key/value pairs. In the case of `scalaVersion`, the key is "scalaVersion"
// and the value is "2.13.12"

// It's possible to define many kinds of settings, such as:

name := "scala_kafka_configurable_producer"
organization := "BlackBoxLunch"
version := "1.0"

// Note, it's not required for you to define these three settings. These are
// mostly only necessary if you intend to publish your library's binaries on a
// place like Sonatype.


// Want to use a published library in your project?
// You can define other libraries as dependencies in your build like this:

libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "2.3.0"

val scala3Version = "3.4.1"

libraryDependencies += "org.apache.kafka" % "kafka-clients" % "3.5.2"
libraryDependencies ++= Seq(
  "org.slf4j" % "slf4j-api" % "1.7.30",
  "ch.qos.logback" % "logback-classic" % "1.2.3"
)
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"


lazy val root = project
  .in(file("."))
  .settings(
    name := "threat-detection-api",
    version := "0.1.0-SNAPSHOT",

    scalaVersion := scala3Version,

    libraryDependencies += "org.scalameta" %% "munit" % "0.7.29" % Test
  )